#!/usr/bin/python

import os
import sys
import hashlib
import getopt
import sys
import shutil
import argparse

dupeVersion = 0.1

opt_ignore_size0 = True

imageTypes = ['3g2', '3gp', 'asf', 'asf',
               'avi', 'avi', 'bmp', 'divx',
               'gif', 'jpg', 'm2t',
               'm2ts', 'm2ts', 'm4v', 'mkv',
               'mmv', 'mmv', 'mod', 'mov', 'mp3',
               'mp4', 'mpg', 'mpg', 'mts', 'png',
               'psd', 'tga', 'tif', 'tiff', 'tod',
               'webp', 'wma', 'wmv', 'wmv' ]

parser = argparse.ArgumentParser()
parser.add_argument("scan", nargs='+', type=str, help="directories to scan for duplicates")
parser.add_argument("--reference", nargs='*', default='', type=str, help="scan these directories first, but never delete anything in these directories")
parser.add_argument('--empty', dest='empty', action='store_false', help='consider zero length files')
parser.add_argument('--size', help='smallest file size to consider in b(ytes), k(ilobytes), M(egabytes), G(igabytes)')
parser.add_argument('--nohardlinks', action='store_true', help='when two or more files point to the same disk area, do not consider them as duplicates')
parser.add_argument('--norecurse', action='store_true', help='do not go into subdirectories')
parser.add_argument('--quiet', action='store_true', help='do not display extra information on progress')
parser.add_argument('--sameline', action='store_true', help='list duplicate files on same line')
parser.add_argument('--nosize', action='store_true', help='do not show the size and hash for duplicate files')
parser.add_argument('--omitfirst', action='store_true', help='omit the first file in each set of matches')
parser.add_argument('--sortbysize', action='store_true', help='sort the duplicate files by file size')
parser.add_argument('--extensions', nargs="+", help='only consider the extensions given. Can provide multiple extensions such as --extensions jpeg jpg gif mp4')
parser.add_argument('--allimages', action='store_true', help='consider all extensions \
                   given by --extension as well as these images extensions: \
                   3g2, 3gp, asf, asf, avi, avi, bmp, divx, gif, jpg, m2t, \
                   m2ts, m2ts, m4v, mkv, mmv, mmv, mod, mov, mp3, mp4, mpg, \
                   mpg, mts, png, psd, tga, tif, tiff, tod, webp, wma, wmv, wmv')
parser.add_argument('--ignoreextensions', default=[], nargs="+", help='do not consider these extensions given. Can provide multiple ignoreextensions to ignore')
parser.add_argument('--ignorefilenames', default=[], nargs="+", help='do not consider these filenames given. If a filename is .DS_Store it needs to be ignored as a file and not as an extension')
parser.add_argument('--ignoredirectories', default=[], nargs="+", help='do not recurse into these directories given, such as .picasaoriginals')
parser.add_argument('--delete', action='store_true', help='delete the duplicates found')
parser.add_argument('--noask', action='store_true', help='do not ask for confirmation when deleting')
parser.add_argument('--version', action='store_true', help='display duplicate-finder version')


parserArgs = parser.parse_args()

opt_ignore_size0 = parserArgs.empty

if parserArgs.allimages:
    # add it to extensions
    if parserArgs.extensions == None:
        parserArgs.extensions = imageTypes
    else:
        parserArgs.extensions += imageTypes


if parserArgs.version:
    print "duplicate-finder ", dupeVersion
    sys.exit(0)
    
opt_minsize = 0
if parserArgs.size:
    lastchar = parserArgs.size[-1:]
    try:
        num = int(parserArgs.size[:-1])
    except ValueError as err:
        print str("arguments to --size must be of the form n[nkMG], such as 100M")
        parser.print_help()
        sys.exit(2)
    if lastchar == 'b':
        opt_minsize = num
    elif lastchar == 'k':
        opt_minsize = 1024 * num
    elif lastchar == 'M':
        opt_minsize = 1024 * 1024 * num
    elif lastchar == 'G':
        opt_minsize = 1024 * 1024 * 1024 * num
    else:
        print str("arguments to --size must be of the form n[nkMG], such as 100M")
        sys.exit(2)

def query_yes_no(question, default="no"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "no" (the default), "yes" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = raw_input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")


#
# All files that match the criteria (size, type, etc) are stored and sorted by their size
# fileBuckets[fileSize] = fileBucket
fileBuckets = {}

fileCounter = 0
duplicateGroupings = 0
duplicateFiles = 0
deletedFiles = 0

# Bucket is either a single file
# or a dict keyed on hash size pointing to a list


# All files in a fileBucket share the same size`
# A bucket can be small or large. A small bucket has a single file with minimal information about the file
# A large bucket has a dictionary based on md5 hashes

class SFile():
    """Storing information about a file"""
    def __init__(self, p, s, i, isReference):
        self.path = p
        self.size = s
        self.inode = i
        self.hash = None
        self.isReference = isReference
    def __str__(self):
        return str(self.path)
        #return str(self.path) + " : " + str(self.size) + " : " + str(self.hash)
    def md5(self):
        hash_md5 = hashlib.md5()
        with open(self.path, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        return hash_md5.hexdigest()
    def addFileHash(self):
        self.hash = self.md5()
        
def addFile(p, isReference=False):
    if os.path.islink(p):
        # do not follow symbolic links
        return
    statinfo = os.stat(p)
    if ((opt_ignore_size0 and statinfo.st_size == 0) or
        (statinfo.st_size < opt_minsize)):
        return
    sf = SFile(p, statinfo.st_size, statinfo.st_ino, isReference)
    # print "Adding file: ", sf, " - ", sf.isReference
    bucket = fileBuckets.get(sf.size)
    if (bucket == None):
        fileBuckets[sf.size] = sf
        return
    # More than one file with the same size
    # Collect file details
    sf.addFileHash()
    if isinstance(bucket,SFile):
        if (sf.path == bucket.path):
            # pointing to the same file, same path
            return
        if (parserArgs.nohardlinks and (sf.inode == bucket.inode)):
            # when two or more files point to the same disk area,
            # do not consider them as duplicates
            # exclude them from the list
            return
        oldfile = bucket
        oldfile.addFileHash()
        # print "B1: ", bucket
        bucket = { oldfile.hash: [ ] }
        bucket[oldfile.hash].append(oldfile)
        # print "B2: ", bucket
        fileBuckets[sf.size] = bucket
    # Bucket is a dict if we get here
    # print "B3: ", bucket    
    bucketGroup = bucket.get(sf.hash)
    if (bucketGroup == None):
        bucket[sf.hash] = [ sf ]
    else:
        # not same path
        if sf.path in [ f.path for f in bucket[sf.hash] ]:
            return            
        # not hardlinked
        if parserArgs.nohardlinks and \
           sf.inode in [ f.inode for f in bucket[sf.hash] ]:
                return
        bucket[sf.hash].append(sf)

def bucketDisplay(bucket):
    """Bucket is a single file or a dict"""
    if isinstance(bucket,SFile):
        print str(bucket)
        return
    # Bucket is a dict
    # each value in the dict is a list of SFiles
    # print "B4: ", bucket
    for buckethash in bucket:
        print "   Hash: ", buckethash
        for f in bucket[buckethash]:
            print "  ", str(f)

def dupeBucketDisplay(bucket):
    global duplicateGroupings
    global duplicateFiles
    global deletedFiles
    if isinstance(bucket,SFile):
        return
    for buckethash in bucket:
        if (len(bucket[buckethash]) > 1):
            duplicateGroupings += 1
            # none of the references are considered duplicates
            # they cannot be deleted, but others can be
            refduplicates = [ sf for sf in bucket[buckethash] if sf.isReference ]
            nonRefduplicates = [ sf for sf in bucket[buckethash] if not sf.isReference ]
            # print "refDuplicates are: " , ", ".join(map(str,refduplicates))            
            # print "NorefDuplicates are: " , ", ".join(map(str,nonRefduplicates))
            # no duplicates, only reference files
            if not nonRefduplicates:
                return
            # if there are are references, add 1, all are duplicates
            duplicateFiles = duplicateFiles + len(nonRefduplicates) - 1
            if refduplicates:
                duplicateFiles += 1
            # if there are are references, grab one if not grab first one
            if refduplicates:
                fst = refduplicates[0]
            else:
                fst = nonRefduplicates[0]
            fullbucketList = refduplicates + nonRefduplicates
            if parserArgs.omitfirst:
                fullbucketList = fullbucketList[1:]
            if parserArgs.sameline:
                if parserArgs.nosize:
                    print "\t",  "\t".join(map(str,fullbucketList))
                else:
                    print "  Size: ", fst.size , "\tHash: ", fst.hash, "\t",  \
                        "\t".join(map(str,fullbucketList))
            else:
                if not parserArgs.nosize:
                    print "  Size: ", fst.size , " Hash: ", fst.hash
                for f in fullbucketList:
                    print "  ", f.path
            print
            if parserArgs.delete:
                if refduplicates:
                    toDelete = nonRefduplicates
                else:
                    toDelete = nonRefduplicates[1:]
                for bb in toDelete:
                    if parserArgs.noask or query_yes_no("Delete " + str(bb)):
                        # delete it safely
                        os.unlink(bb.path)
                        deletedFiles += 1
                        print "Deleted " , bb.path

def duplicateDisplay():
    """Display duplicates in fileBuckets"""
    if parserArgs.sortbysize:
        sizeKeys = fileBuckets.keys()
        sizeKeys.sort(reverse=True)
        for k in sizeKeys:
            dupeBucketDisplay(fileBuckets[k])
    else:
        for k in fileBuckets:
            dupeBucketDisplay(fileBuckets[k])
        
fileSkipCounter = 0
crossdevice = False

def filesize(filename):
    statinfo = os.stat(filename)
    return statinfo.st_size

def endStats():
    print "Files found: ", fileCounter
    # print "Groups of duplicates: ", duplicateGroupings
    print "Duplicate files: ", duplicateFiles
    print "Deleted files: ", deletedFiles    

def ignoringDirectory(dir):
    return true;

def startScan(start, isReference=False):
    print "Startscan with ", isReference
    global fileCounter
    counter = 0
    dotcounter = 0
    magnitude = 100
    for root, dirs, files in os.walk(start, topdown=False):
        print "R: ", root, "d: ", dirs, " f: ", files
        head, tail = os.path.split(root)
        if tail in parserArgs.ignoredirectories:
            print "Skipping this one"
            continue;
        for name in files:
            f = os.path.join(root, name)
            if not parserArgs.quiet:
                if (dotcounter > magnitude):
                    sys.stdout.write(".")
                    sys.stdout.flush()                
                    dotcounter = 0
            if (counter >= magnitude * 10):
                if not parserArgs.quiet:
                    sys.stdout.write("\rScanned " + str(fileCounter) + " files")
                counter = 0
            counter += 1
            dotcounter += 1
            fileCounter += 1
            froot,fext = os.path.splitext(f)
            fext = fext.lower()
            if (parserArgs.extensions == None or fext in parserArgs.extensions) and \
               (not fext in parserArgs.ignoreextensions) and \
               (not os.path.basename(froot) in parserArgs.ignorefilenames) :
                print "Adding file root: " , froot, " ext: ", fext, " ignores are: ", "::".join(parserArgs.ignorefilenames)
                addFile(f, isReference)
        if parserArgs.norecurse:
            return

refdirs = [ os.path.realpath(f) for f in parserArgs.reference ]
starts = [ os.path.realpath(f) for f in parserArgs.scan ]

for s in refdirs:
    if not os.path.isdir(s):
        print s, " is not a directory"
        sys.exit(2)
        
for s in starts:
    if not os.path.isdir(s):
        print s, " is not a directory"
        sys.exit(2)

for s in refdirs:
    startScan(s, True)        
        
try:
    for s in starts:
        startScan(s)
    print "========="
    print "Scanned ", "\t".join(map(str,starts))
    duplicateDisplay()
    endStats()
except KeyboardInterrupt, e:
    print str(e)
    endStats()



