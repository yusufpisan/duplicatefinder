# README #

duplicateFinder is a python based tool for finding duplicate files and deleting them. It works especially well for pictures.

### What is this repository for? ###

* Keeping track of duplicateFinder


### How do I get set up? ###

* duplicate-finder.py --help will give you all the command line options


### Examples ###

* duplicate-finder.py /Users/me/Pics/ --ignoredirectories .picasaoriginals

* duplicate-finder.py /Users/me/Pics/ --reference /Users/me/OldPics --allimages --ignoreextensions ini

* duplicate-finder.py /Users/me/Pics/ --reference /Users/me/OldPics --allimages --ignoreextensions ini --delete




### Who do I talk to? ###

* Yusuf Pisan yusuf.pisan@gamesstudio.org